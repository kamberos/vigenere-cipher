﻿using System;

namespace vigenereCipher
{
    class CipherTools
    {
        // if Cipher key is too short or to long for plaintext
        public static string KeyCipherGenerator(string input, string keyCipher)
        {
            if(input.Length > keyCipher.Length)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    if(input.Length == keyCipher.Length)
                    {
                        return keyCipher;
                    }
                     
                    keyCipher += keyCipher[i];
                    
                }

            }
            return keyCipher;
        }
        public static string Ciphering(string input, string keyCipher)
        {
            string Cipher = string.Empty;

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i].ToString().Equals(" "))
                {
                    Cipher += " ";
                }

                else
                {
                    int Cipherletter = ((input[i] + keyCipher[i]) % 26);
                    Cipherletter += 'a';
                    Cipher += (char)Cipherletter;
                }
            }

            return Cipher;
        }
        public static string Deciphering(string cipher, string keyCipher)
        {
            string decipher = string.Empty;

            for (int i = 0; i < cipher.Length ; i++)
            {
                if (cipher[i].ToString().Equals(" "))
                {
                    decipher += " ";
                }
                else
                {
                    int decipherletter = ((cipher[i] - keyCipher[i] + 26) % 26);
                    decipherletter += 'a';
                    decipher += (char)decipherletter;
                }
            }
            return decipher;
        } 

    }
    class Program
    {
        static void Main(string[] args)
        {
            string keyCipher = "tajne";
            string input = "ala ma super kota felka";

            keyCipher = CipherTools.KeyCipherGenerator(input,keyCipher);
            Console.WriteLine("Plain Text: {0}", input);
            
            string CipherText = CipherTools.Ciphering(input.ToUpper(), keyCipher.ToUpper());
            Console.WriteLine("Ciphered Text: {0}", CipherText);

            Console.WriteLine(CipherTools.Deciphering(CipherText.ToUpper(), keyCipher.ToUpper()));
        }
        
    }
}
